import java.util.Scanner;
public class BoardGameApp{
		public static void main(String[] args){
				Scanner reader = new Scanner(System.in);
				Board myGame = new Board();


				System.out.println("Welcome to this game, you will play in the following grid:\n"+myGame+"\nPress enter when you're ready...");
				reader.nextLine();

				int numCastles = 7;
				int turns = 0;

				while (numCastles > 0 && turns < 8){
						System.out.println("Beginning of turn " + turns + ". There are " + numCastles + " castles... Here's how the game is going: \n"+myGame);
						System.out.println("Enter Row:");
						int row = Integer.parseInt(reader.nextLine());
						System.out.println("Enter Column:");
						int column = Integer.parseInt(reader.nextLine());

						int tokenResult = myGame.placeToken(column,row);

						while (tokenResult < 0){
								System.out.println("Invalid column or row, please input again...");

								System.out.println("Enter Row:");
								row = Integer.parseInt(reader.nextLine());
								System.out.println("Enter Column:");
								column = Integer.parseInt(reader.nextLine());
								tokenResult = myGame.placeToken(column,row);
						}

						if (tokenResult == 1){
								System.out.println("There was a wall... no Castle placed...");
						}else {
								System.out.println("A Castle Was Placed!");
								numCastles --;
						}
						turns ++;

		}
		System.out.println(myGame);
		if (numCastles == 0){
				System.out.println("YOU WON!");
		} else {
				System.out.println("YOU LOST...");
		}
		}


}
