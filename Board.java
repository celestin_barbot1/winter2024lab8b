import java.util.Random;
public class Board{
		private Tile[][] grid;
		private final int SIZE = 5;
		private Random rand;
		public Board(){
				this.grid = new Tile[this.SIZE][this.SIZE];
				rand = new Random();
				int randIndex;

				for(int i=0; i<this.grid.length;i++){
					randIndex =	rand.nextInt(this.grid[i].length);
					for (int j=0; j < this.grid[i].length; j++){
							this.grid[i][j] = Tile.BLANK;
					}
					this.grid[i][randIndex] = Tile.HIDDEN_WALL; // Change if not enough hidden walls
				}
		}
		public String toString(){
				String table = "";
				for (int i=0; i<this.grid.length;i++){
						for (int j=0; j<this.grid[i].length;j++){
								table += "|" + this.grid[i][j].getName();
						}
						table += "|\n";
				}
				return table;
		}
		public int placeToken(int col, int row){
				if(col > this.SIZE || col <= 0 || row > this.SIZE || row <= 0 ){
						return -2;
				}else if ((this.grid[row-1][col-1] == Tile.CASTLE || this.grid[col-1][row-1] == Tile.WALL)){	  
						return -1;
				}else if (this.grid[row-1][col-1] == Tile.HIDDEN_WALL){
						this.grid[row-1][col-1] = Tile.WALL;
						return 1;
				}else {
						this.grid[row-1][col-1] = Tile.CASTLE;
						return 0;
				}

		}
		 

}
